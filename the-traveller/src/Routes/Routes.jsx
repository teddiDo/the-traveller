import { Switch, Route } from "react-router-dom"
import Home from "./Home"
import Login from "../Components/Login/Login"
import SignUp from "../Components/Login/Signup"
import SearchPage from "../Components/SearchPage/SearchPage"
import TourDetails from "../Components/TourDetalis/TourDetails"

export default function WebRoutes() { 

    return <>
        <Switch>
            <Route exact path="/">
                <Home />
            </Route>
            <Route exact path="/login">
                <Login />
            </Route>
            <Route exact path="/signup">
                <SignUp />
            </Route>
            <Route exact path="/search">
                <SearchPage />
            </Route>
            <Route exact path="/details">
                <TourDetails />
            </Route>
        </Switch>
    </>

}