import React from 'react';
import WebFooter from '../Components/Footer/WebFooter';
import PopularSearch from '../Components/Popular/PopularSearch';
import NavBar from "../Components/Navbar/NavBar";
import MiddlePicture from "../Components/MiddlePicture/MiddlePicture"
import TravelTips from "../Components/Tips/TravelTips"
import Recommendations from '../Components/Recommendations/Recommendations';
import HeadPicture from '../Components/HeadPicture/HeadPicture';
import SearchBar from '../Components/SearchBar/SearchBar';
import MiddleSection from '../Components/middleSection/MiddleSection';
import styles from "./Home.module.css"
export default function Home() {
  return (
    <div className={styles.home}>
        <NavBar />
        <HeadPicture />
        <SearchBar />
        <PopularSearch />
        <MiddleSection />
        <MiddlePicture />
        <Recommendations />
        <TravelTips />
        <WebFooter />
    </div>
  );
}