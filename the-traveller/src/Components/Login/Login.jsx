import React from "react";
import styles from "./Login.module.css"
import { Link } from "react-router-dom";
import TextField  from "@mui/material/TextField";
import Button from "@mui/material/Button"

export default function Login() {
    return (
        <div className={styles.v312_4}>
            <div className={styles.v317_1692}>
                <div className={styles.v317_1675}>
                    <Button
                        className={styles.v317_1676}
                        variant="contained"
                    >
                        Sign in
                    </Button>
                </div>

                <div className={styles.v317_1694}>
                    <Link to="/">
                        <span class={styles.v317_1668}>The Traveller</span>
                    </Link>
                    <span className={styles.v317_1679}>Sign in</span>
                    <div className={styles.name}></div>
                </div>
                <div className={styles.v317_1684}>
                    <TextField 
                        className={styles.v317_1669}
                        placeholder="Email address"
                    />
                </div>
                <div className={styles.v317_1685}>
                    <TextField 
                        className={styles.v317_1670}
                        placeholder="Password"
                    />
                </div>
                <div className={styles.v317_1693}>
                    <div className={styles.v317_1686}></div>
                    <div className={styles.v317_1687}></div>
                    <div className={styles.v317_1688}></div>
                </div>
                <Link to="/signup">
                    <span className={styles.v317_1690}>Create an account with us!</span>
                </Link>
                <span className={styles.v317_1691}>Forgot password?</span>
            </div>
        </div>
    );
}