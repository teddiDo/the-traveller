import React from "react";
import styles from "./Signup.module.css"
import { Link } from "react-router-dom";
import TextField from "@mui/material/TextField"
import Button from "@mui/material/Button"

export default function SignUp() {
  return (
    <div className={styles.v317_1695}>
      <div className={styles.v317_1696}>
        <div className={styles.v317_1703}>
          <Link to="/">
            <span className={styles.v317_1704}>The Traveller</span>
          </Link>
          
          <span className={styles.v317_1705}>Sign up</span>
          <div className={styles.name}></div>
        </div>
        <div className={styles.v431_14}>
          <TextField
            className={styles.v431_15}
            placeholder="Nick name"
          />
        </div>
        <div className={styles.v317_1700}>
          <TextField
            className={styles.v317_1701}
            placeholder="Email address"
          />
        </div>
        <div className={styles.v317_1707}>
          <TextField
            className={styles.v317_1708}
            placeholder="Password"
          />
        </div>
        <div className={styles.v317_1717}>
          <TextField
            className={styles.v317_1718}
            placeholder="Repeat password"
          />
        </div>
        <div className={styles.v317_1697}>
          <Button
            className={styles.v317_1698}
            variant="contained"
          >
            Sign up
          </Button>
        </div>

        <div className={styles.v317_1710}>
          <div className={styles.v317_1711}></div>
          <div className={styles.v317_1712}></div>
          <div className={styles.v317_1713}></div>
        </div>
        <div className={styles.v317_1716}>
          <Link to="/login">
            <span className={styles.v317_1714}>Sign in with your account!</span>
          </Link>
          <span className={styles.v317_1715}>Forgot password?</span></div>
      </div>
    </div>
  );
}

