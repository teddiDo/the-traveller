import React from "react";
import styles from "./Card.module.css"
import Button from "@mui/material/Button";
import Box from "@mui/material/Box"
import { Link } from "react-router-dom";
export default function Card() {
    return (
        <Box className={styles.card}>
            <img src="/assets/tour.png" className={styles.image} alt={"Tour"} />
            <div className={styles.info}>
                <div>
                    <h2>Volendam, Marken, Edam and Windmills Day Trip from Amsterdam </h2>
                    <div>4
                        <img src="/assets/star.png" alt="" width="20px" />
                        (35)
                    </div>
                    <div>
                        Volendam - Marken - Edam
                    </div>
                </div>

                <span>
                    Leave Amsterdam behind for a few hours and discover
                    the typical Dutch countryside outside the capital. You'll drive through a
                    picture-perfect landscape crisscrossed with canals, see authentic wooden houses,
                    windmills and fishing villages, and witness traditional crafts from days gone by
                    ...
                </span>

                <div className={styles.horizon}>
                    <div className={styles.policy}>
                        <span>4 SEATS AVAILABLE!</span>
                        <span>FREE CANCELLATION</span>
                    </div>
                    <Link to="/details">
                        <Button
                            className={styles.button}
                            variant="contained"
                        >
                            $134.5/PERSON {" > "}
                        </Button>
                    </Link>
                </div>

            </div>


        </Box>
    );
}