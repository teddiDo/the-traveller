import React from "react";
import { Link } from "react-router-dom";
import NavBar from "../Navbar/NavBar";
import WebFooter from "../Footer/WebFooter";
import SearchBar from "../SearchBar/SearchBar";
import styles from "./SearchPage.module.css"
import Pagination from "@mui/material/Pagination";
import PopularSearch from "../Popular/PopularSearch"
import FilterFeature from "./Filter/FilterFeature";
import ToursSearch from "./ToursSearch/ToursSearch";

export default function SearchPage() {
    return (

        <div className={styles.flex}>
            <NavBar />

            <SearchBar
                page={"search-page"}
            />

            <div className={styles.horizon}>
                <FilterFeature />
                <ToursSearch />
            </div>


            <Pagination count={10} size="large" className={styles.v354_30} />
            {/* <PopularSearch 
                page={"search-page"}
            /> */}
            <div className={styles.name}></div>
            <WebFooter
                page={"search-page"}
            />

        </div>
    );
}
