import React from "react";
import styles from "./FilterFeature.module.css"
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Box from '@mui/material/Box';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
export default function FilterFeature() {
    const steps = [
        "< 500$",
        "< 1000$",
        "> 1000$",
        "> 1500$"
    ];
    return (
        <div className={styles.v375_135}>
            <h1 className={styles.h1}>Filter</h1>
            <FormGroup>
                <h3 className={styles.h4}>Budget</h3>
                <Box sx={{ width: '100%' }}>
                    <Stepper activeStep={1} alternativeLabel>
                        {steps.map((label) => (
                            <Step key={label}>
                                <StepLabel>{label}</StepLabel>
                            </Step>
                        ))}
                    </Stepper>
                </Box>
            </FormGroup>
            <FormGroup className={styles.h4}>
                <h3>Policies</h3>
                <FormControlLabel control={<Checkbox defaultChecked />} label="Free cancellation" />
            </FormGroup>
            <FormGroup className={styles.h4}>
                <h3>Your account</h3>
                <FormControlLabel control={<Checkbox defaultChecked />} label="Your registers" />
            </FormGroup>
            <FormGroup className={styles.h4}>
                <h3>Rating</h3>
                <FormControlLabel control={<Checkbox defaultChecked />} label={
                    <>
                        1 <img src="/assets/star.png" alt="" width="15px" />
                    </>
                }/>
                <FormControlLabel control={<Checkbox defaultChecked />} label="2" />
                <FormControlLabel control={<Checkbox defaultChecked />} label="3" />
                <FormControlLabel control={<Checkbox defaultChecked />} label="4" />
                <FormControlLabel control={<Checkbox defaultChecked />} label="4" />
            </FormGroup>

            <FormGroup className={styles.h4}>
                <h3>Locations</h3>
                <FormControlLabel control={<Checkbox defaultChecked />} label="Europe" />
                <FormControlLabel control={<Checkbox defaultChecked />} label="Asia" />
                <FormControlLabel control={<Checkbox defaultChecked />} label="Africa" />
                <FormControlLabel control={<Checkbox defaultChecked />} label="Australia/New Zealand" />
                <FormControlLabel control={<Checkbox defaultChecked />} label="America" />
            </FormGroup>
        </div>
    );
}
