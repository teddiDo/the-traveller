import './App.css';
import React from "react";
import Carousel from "react-elastic-carousel";
import Item from "./Item";
import Europe from "./image/europe.png"
import Asia from "./image/asia.png"
import Africa from "./image/africa.png"
import Aus from "./image/aus.png"
import America from "./image/america.png"

const breakPoints = [
  { width: 1, itemsToShow: 1 },
  { width: 1, itemsToShow: 1 },
  { width: 550, itemsToShow: 2, itemsToScroll: 1, pagination: false },
  { width: 768, itemsToShow: 3, itemsToScroll: 1, pagination: false },
  { width: 800, itemsToShow: 4, itemsToScroll: 1, pagination: false }
];

export default function MiddleSection() {
  return (
    <div className="App">
      <div className="corosel rec.rec-arrow:hover">
        <span className="v340_45">Locations</span>
        <Carousel breakPoints={breakPoints} pagination={false} >
          <Item>
            <div className="divimg">
              <img className="browsimg" src={Europe} alt="Europe" />
              <div class="v324_324"></div>
            </div>
            <div className="diver">
              <h4 className="subhead">Europe</h4>
              <p className="bbrowssub">802,405 tours</p>
            </div>
          </Item>

          <Item>
            <div className="divimg">
              <img className="browsimg" src={Asia} alt="Asia" />
            </div>
            <div className="diver">
              <h4 className="subhead">Asia</h4>
              <p className="bbrowssub">807,884 tours</p>
            </div>
          </Item>

          <Item>
            <div className="divimg">
              <img className="browsimg" src={Africa} alt="Africa" />
            </div>
            <div className="diver">
              <h4 className="subhead">Africa</h4>
              <p className="bbrowssub">17,482 tours</p>
            </div>
          </Item>

          <Item>
            <div className="divimg">
              <img className="browsimg" src={Aus} alt="Aus/NZ" />
            </div>
            <div className="diver">
              <h4 className="subhead">Aus/NZ</h4>
              <p className="bbrowssub">406,281 tours</p>
            </div>
          </Item>

          <Item>
            <div className="divimg">
              <img className="browsimg" src={America} alt="America" />
            </div>
            <div className="diver">
              <h4 className="subhead">America</h4>
              <p className="bbrowssub">31,734 tours</p>
            </div>
          </Item>

        </Carousel>
      </div>

      <div >
      </div>

    </div>
  );
}