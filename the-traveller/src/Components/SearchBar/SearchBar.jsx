import React from "react";
import styles from "./SearchBar.module.css"
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { Link } from "react-router-dom";
import Calendar from "react-calendar";
import 'react-calendar/dist/Calendar.css';
import Box from "@mui/material/Box"

export default function SearchBar({ page }) {
    return (
        <div className={styles.search}>
            <div className={page === "search-page" ? styles.searchpage : styles.home}>
                <TextField
                    className={styles.v324_206}
                    placeholder="Where do you travel?"
                    variant="outlined"
                />
                <Box className={styles.v324_196}>
                    <div>
                        <span>
                            From:
                            {" "}
                            <span>Mar 3 2021</span>
                            {" / "}
                            To: {" "}
                            <span>Mar 5 2021</span>
                        </span>
                    </div>
                    {/* <Calendar /> */}
                </Box>
                <Box className={styles.v324_196}>
                    <div>
                        <span> 1 Adults and 2 childrens</span>
                    </div>
                    {/* <div className={styles.filter}>
                            <h4>Children</h4>
                            <div >
                                <div>
                                    <Button variant="outlined">-</Button>
                                </div>
                                <div>
                                    <p>4</p>
                                </div>
                                <div>
                                    <Button variant="outlined">+</Button>
                                </div>
                            </div>
                            <h4>Adult</h4>
                            <div >
                                <div>      
                                <Button variant="outlined">-</Button>
                                </div>
                                <div>
                                    <p>4</p>
                                </div>
                                <div>
                                <Button variant="outlined">+</Button>
                                </div>
                            </div>
                    </div> */}
                </Box>

                <Link to="/search">
                    <Button
                        className={styles.v324_210}
                        variant="contained"
                    >
                        Search
                    </Button>
                </Link>
            </div>
        </div>
    );
}

