import React from "react";
import styles from "./NavBar.module.css"

import { Link } from "react-router-dom";
export default function NavBar() {

    return ( 
        <div className={styles.v324_123}>
            <div className={styles.v324_124}></div>
            <Link to="/">
                <span className={styles.v324_125}>The Traveller</span>
            </Link>
            <div className={styles.v324_129}>
                <Link to="/signup">
                    <div className={styles.v324_130}></div><span className={styles.v324_131}>Sign up</span>
                </Link>
            </div>
            <div className={styles.v382_1017}>
                <Link to="/login">
                    <div className={styles.v382_1018}></div><span className={styles.v382_1019}>Sign in</span>
                </Link>
            </div>

            {/* <span className={styles.v324_125}>Thành</span> */}

        </div>
     );
}