import React from "react";
import styles from "./TravelTips.module.css"

export default function TravelTips() {
    return ( 
        <div className={styles.v340_31}>
        <div className={styles.v340_32}></div>
        <div className={styles.v340_33}>
            <div className={styles.v340_34}></div>
            <div className={styles.v340_35}>
                <span className={styles.v340_36}>Don’t be afraid to use a map.</span>
                <span className={styles.v340_37}>
                    Looking like a tourist isn’t as bad as getting really lost and ending up in a
                    different directions. Don’t be afraid to use a map or ask for directions and look like a
                    tourist. You should always use a map when you travel. It helps you get to where you need to go!
                </span>
            </div>
        </div>
        <div className={styles.v340_38}>
            <div className={styles.v340_39}></div>
            <div className={styles.v340_40}>
                <span className={styles.v340_41}>Travel by yourself at least once</span>
                <span className={styles.v340_42}>You’ll learn a lot about yourself and how to become independent. You’ll learn
                    valuable life skills when you push yourself!
                </span>
            </div>
        </div>
        </div> 
     );
}