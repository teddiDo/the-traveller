import React from "react";
import styles from "./WebFooter.module.css";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button"

export default function WebFooter({ page }) {
    return (
        <div className={page === "search-page" ? styles.searchpage : styles.home}>
            <div className={styles.v331_4}></div>
            <div className={styles.v331_5}>
                <span className={styles.v331_6}>Subcribe us!</span>
                <div className={styles.v331_7}>
                    <TextField 
                        className={styles.v331_8}
                        placeholder="Email address"
                    />
                </div>

                <div className={styles.v331_10}>
                    <Button className={styles.v331_11}>
                        Subcribe
                    </Button>
                </div>
                
            </div>

            <div className={styles.v331_13}>
                <div className={styles.v331_14}>
                    The Traveller
                    <hr className={styles.name}/>
                </div>
            </div>

            <div className={styles.v331_16}>
                <div className={styles.v331_17}>
                    <h2>Locations</h2>
                    <span>Europe</span>
                    <span>Asia</span>
                    <span>Africa</span>
                    <span>Australia/New Zealand</span>
                    <span>America</span>
                </div>
                <div className={styles.v331_24}>             
                    <h2>Project</h2>
                    <span>About me!</span>
                    <span>Documentation</span>
                    <span>Contact me!</span>
                    <span>Signed by Thanh Do</span>
                </div>
            </div>
        </div>
    );
};
