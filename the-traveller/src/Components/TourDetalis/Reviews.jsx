import React, { useEffect, useState } from "react";
import { makeStyles, withStyles } from "@mui/styles";
import Slider from "@mui/material/Slider"
import styled from "styled-components";
import Skeleton from "@mui/material/Skeleton";

const PrettoSlider = withStyles({
    root: {
        alignItems: "center",
        padding: "12px 0",
    },
    valueLabel: {
        left: "calc(-50% + 4px)",
    },
    track: {
        height: 8,
        borderRadius: 4,
        color: "rgb(2,128,0)",
    },
    rail: {
        height: 8,
        borderRadius: 4,
        color: "#b7d7f7",
    },
})(Slider);

const useStyle = makeStyles({
    logoWidth: {
        width: "20px",
        height: "20px",
    },
    colo: {
        backgroundColor: "white",
        width: "50%",
        paddingLeft: "7%",
        marginTop: "10px",
        marginBottom: "12px"
    },
});
const ReviewMain = ({ x, r }) => {
    const [load, setLoad] = useState(true);
    const cls = useStyle();
    useEffect(() => {
        setTimeout(() => {
            setLoad(false);
        }, Math.random() * 3000);
    }, []);
    return load ? (
        <div className={cls.colo}>
            <Tag>
                <Skeleton animation="wave" width="30%" />
                <Review>
                    <div>
                        <h1>
                            <Skeleton animation="wave" width="30%" />
                        </h1>
                        <p>
                            <Skeleton animation="wave" />
                        </p>
                    </div>
                    <div>
                        <div>
                            <p>
                                <Skeleton animation="wave" width="30%" />
                            </p>
                            <p>
                                <Skeleton animation="wave" width="50%" />
                            </p>
                            <p>
                                <Skeleton animation="wave" width="30%" />
                            </p>
                            <p>
                                <Skeleton animation="wave" width="40%" />
                            </p>
                            <p>
                                <Skeleton animation="wave" width="60%" />{" "}
                            </p>
                        </div>
                        <div>
                            <p>
                                <Skeleton animation="wave" width="100%" />
                            </p>
                            <p>
                                <Skeleton animation="wave" width="100%" />
                            </p>
                            <p>
                                <Skeleton animation="wave" width="100%" />
                            </p>
                            <p>
                                <Skeleton animation="wave" width="100%" />
                            </p>
                            <p>
                                <Skeleton animation="wave" width="100%" />
                            </p>
                        </div>
                        <div>
                            <p>
                                <Skeleton animation="wave" width="100%" />
                            </p>
                            <p>
                                <Skeleton animation="wave" width="100%" />
                            </p>
                            <p>
                                <Skeleton animation="wave" width="100%" />
                            </p>
                            <p>
                                <Skeleton animation="wave" width="100%" />
                            </p>
                            <p>
                                <Skeleton animation="wave" width="100%" />
                            </p>
                        </div>
                    </div>
                </Review>
            </Tag>
        </div>
    ) : (
        <div className={cls.colo}>
            <Review>
                <div>
                    <div>
                        <h2>{r} 4.5</h2>
                        <p>
                            <b>Over 35 reviews</b>
                        </p>
                    </div>
                    <div>
                        5 stars
                        <PrettoSlider disabled value={1 * 10} />
                        4 stars
                        <PrettoSlider disabled value={2 * 10} />
                        3 stars
                        <PrettoSlider disabled defaultValue={3 * 10} />
                        2 stars
                        <PrettoSlider disabled defaultValue={4 * 10} />
                        1 stars
                        <PrettoSlider disabled defaultValue={5 * 10} />
                    </div>
                </div>
            </Review>
        </div>
    );
};

export default ReviewMain;

const Review = styled.div`
//   width: 100%;
  display: flex;
  padding: 20px;
  border: 1px solid rgb(205, 208, 210);
  > :nth-child(1) {
    flex: 1;
    h2 {
      background-color: rgb(0, 95, 0);
      color: white;
      width: 60px;
      font-size: 26px;
      text-align: center;
      padding: 2px 5px;
      border-radius: 20px;
      margin-bottom: 15px;
    }
    p {
      font-size: 13px;
      :hover {
        text-decoration: underline;
      }
    }
  }
  > :nth-child(2) {
    flex: 2;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    > :nth-child(1) {
      flex: 1.2;
      border-left: 1px solid rgb(205, 208, 210);
      padding-left: 10px;
    }
    > :nth-child(3) {
      flex: 1;
      text-align: end;
    }
    > :nth-child(2) {
      width: 50px;
      flex: 1.8;
    }
    p {
      font-size: 14px;
      padding: 6px;
    }
  }
`;
const Tag = styled.h2`
  padding: 20px 15px 10px;
  font-size: 18px;
  font-weight: 600;
`;