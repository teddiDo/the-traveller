import React from "react";
import WebFooter from "../Footer/WebFooter"
import styles from "./TourDetails.module.css"
import NavBar from "../Navbar/NavBar";
import Button from "@mui/material/Button"
import Reviews from "./Reviews"

export default function TourDetails() {
    return (
        <div className={styles.details}>
            <NavBar />
            <h1 className={styles.title}>Volendam, Marken, Edam and Windmills Day Trip from Amsterdam</h1>
            <div className={styles.reimg}>
                <div className={styles.images}>
                    <img src="/assets/tour.png" alt="Tour" width="310px" />
                    <div>
                        <img src="/assets/tour.png" alt="Tour" width="276px" height="124px" />
                        <img src="/assets/tour.png" alt="Tour" width="276px" height="124px" />
                    </div>
                    <img src="/assets/tour.png" alt="Tour" width="310px" />
                </div>
                <div className={styles.register}>
                    <h2>Join tour</h2>
                    <div>
                        <span class="v378_589">Number of people</span>
                        <span class="v378_590">-</span>
                        <span class="v378_590">20</span>
                        <span class="v378_590">+</span>
                    </div>
                    <div>
                        <span class="v378_589">Price</span>
                        <span class="v380_638">$134.5 / PERSON</span>
                    </div>
                    <Button
                        className={styles.button}
                        variant="contained"
                    >
                        Register
                    </Button>
                </div>
            </div>

            <div className={styles.reimg}>
                <div
                    className={styles.overview}>
                    <h2>Overview</h2>
                    <span class="v378_466">
                        Leave Amsterdam behind for a few hours and discover the typical
                        Dutch countryside outside the capital. You'll drive through a picture-perfect landscape crisscrossed
                        with canals, see authentic wooden houses, windmills and fishing villages, and witness traditional crafts
                        from days gone.
                    </span>

                    <h4>Volendam {" / "} Marken {" / "} Edam</h4>
                </div>

                <div className={styles.policy}>
                    <h2>Policies</h2>
                    <span>+ Free cancellation</span>
                    <span class="v378_627">4 Seats Available!</span>
                </div>
            </div>
            <div className={styles.description}>
                <h2>Description</h2>
                <div className={styles.viewmore}>
                    <div>
                        Day 1:
                        Leave Amsterdam
                        behind for a few hours and discover the typical Dutch countryside outside the capital.
                        You'll drive through a picture-perfect landscape crisscrossed with canals, see authentic
                        wooden houses, windmills and fishing villages, and witness traditional crafts from days gone
                        by ...
                    </div>
                    <div>
                        Day 2:
                        Leave Amsterdam
                        behind for a few hours and discover the typical Dutch countryside outside the capital.
                        You'll drive through a picture-perfect landscape crisscrossed with canals, see authentic
                        wooden houses, windmills and fishing villages, and witness traditional crafts from days gone
                        by ...
                    </div>

                </div>
                <div class="name"></div>
                <span class="v378_495">View more</span>
                <hr />
            </div>
            <Reviews />

            <WebFooter />
        </div>
    );
}

