import React from "react";
import styles from "./Recommendations.module.css"

export default function Recommendations() {
    return (
        <div className={styles.v347_103}>
            <div className={styles.v340_47}></div>
            <div className={styles.v347_102}><span className={styles.v340_51}>Recommendations</span>
                <div className={styles.v347_95}>
                    <div className={styles.v340_52}></div>
                    <div className={styles.v347_94}><span className={styles.v340_62}>Tokyo</span>
                        <div className={styles.v340_65}></div><span className={styles.v344_69}>The world's most populous metropolis.</span>
                    </div>
                </div>
                <div className={styles.v347_97}>
                    <div className={styles.v340_53}></div>
                    <div className={styles.v347_96}><span className={styles.v346_70}>Paris</span>
                        <div className={styles.v346_74}></div><span className={styles.v346_77}>The global center for art, fashion, gastronomy
                            and culture.</span>
                    </div>
                </div>
                <div className={styles.v347_99}>
                    <div className={styles.v340_54}></div>
                    <div className={styles.v347_98}><span className={styles.v346_83}>New York</span><span className={styles.v346_84}>The world’s major
                        commercial, financial, and cultural city.</span>
                        <div className={styles.v346_89}></div>
                    </div>
                </div>
                <div className={styles.v347_101}>
                    <div className={styles.v340_55}></div>
                    <div className={styles.v347_100}>
                        <span className={styles.v347_90}>Hanoi</span>
                        <div className={styles.v347_92}></div>
                        <span className={styles.v347_93}>The city of centuries-old architecture and cultures.</span>
                    </div>
                </div>
            </div>
        </div>
    );
}

