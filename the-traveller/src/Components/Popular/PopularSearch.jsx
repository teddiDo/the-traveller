import React from "react";
import styles from "./PopularSearch.module.css"

export default function PopularSearch({ page }) {

    return (
        <div className={page === "search-page" ? styles.searchpage : styles.v334_17}>
            <div className={styles.v334_18}>Popular Search</div>
            <div className={styles.row}>
                <div className={styles.v334_19}>
                    <span>Egypt</span>
                    <span>Ha Long Bay</span>
                    <span>Paris</span>
                    <span>Hoi An</span>
                </div>
                <div className={styles.v334_19}>
                    <span>Milan</span>
                    <span>Niagara Falls</span>
                    <span>Amsterdam</span>
                    <span>Tokyo</span>
                </div>
            </div>
        </div>
    );
}