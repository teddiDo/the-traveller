# The Traveller

## About
Status: Updating
Backend: Updating
Frontend: Updating
Connection (Frontend and backend): Updating

## Technologies Used
+ ReactJs, NodeJS

## Frontend Design with Figma
![First Design](./the-traveller/public/assets/prototype.png)
## Contributer
+ @teddiDo

## Contacts
+ Student email: 465288@student.saxion.nl
+ Work email: ducthanhdo.developer@gmail.com
