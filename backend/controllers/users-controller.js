const HttpError = require('../models/http-error');
const {validationResult} = require('express-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../models/users-model');

const getUsers = async (req, res, next) => {
  let users;
  try {
    users = await User.find({}, '-password');
  } catch (error) {
    return next(
        new HttpError('Could not fetch users.', 500),
    );
  }
  // eslint-disable-next-line max-len
  return res.status(200).json({users: users.map((user) => user.toObject({getters: true}))});
};

const signUp = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(
        new HttpError('Invalid information on the request body', 422),
    );
  }
  const {name, email, password} = req.body;
  let existingUser;
  try {
    existingUser = await User.findOne({email: email});
  } catch (error) {
    return next(
        new HttpError('Could not sign up.', 500),
    );
  }
  if (existingUser) {
    return next(
        new HttpError('This email already exists.', 422),
    );
  }
  let hashedPassword;
  try {
    hashedPassword = await bcrypt.hash(password, 12);
  } catch (error) {
    return next(
        new HttpError(
            'Could not sign up, please try again.', 500,
        ),
    );
  }
  const createdUser = new User({
    name,
    email,
    password: hashedPassword,
    tours: [],
  });

  let token;
  try {
    token = jwt.sign(
        {
          userId: createdUser.id,
          name: createdUser.name,
          email: createdUser.email,
        },
        'this_is_a_secret_key',
        {expiresIn: '10h'});
  } catch (error) {
    return next(
        new HttpError(
            'Could not sign up.', 500,
        ),
    );
  }
  try {
    await createdUser.save();
  } catch (error) {
    return next(
        new HttpError(
            'Could not sign up new user.', 500,
        ),
    );
  }
  // eslint-disable-next-line max-len
  return res.status(201).json({createdUser: createdUser.toObject({getters: true}), token: token});
};

const login = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    throw new HttpError('Invalid information on the request body', 422);
  }
  const {email, password} = req.body;

  let existingUser;
  try {
    existingUser = await User.findOne({email: email});
  } catch (error) {
    return next(
        new HttpError('Could not login.', 500),
    );
  }
  if (!existingUser) {
    return next(
        new HttpError('Invalid Credentials', 401),
    );
  }
  let isValidPassword = false;
  try {
    isValidPassword = await bcrypt.compare(password, identifiedUser.password);
  } catch (error) {
    return next(
        new HttpError('Could not log in', 500),
    );
  }
  if (!isValidPassword) {
    return next(
        new HttpError('Invalid Credentials', 401),
    );
  }
  let token;
  try {
    token = jwt.sign(
        {
          userId: existingUser.id,
          name: existingUser.name,
          email: existingUser.email,
        },
        'this_is_a_secret_key',
        {expiresIn: '10h'});
  } catch (error) {
    return next(
        new HttpError(
            'Could not sign in, please try again.', 500,
        ),
    );
  }
  return res.status(200).json({
    userId: existingUser.id,
    token: token,
  });
};

exports.getUsers = getUsers;
exports.signUp = signUp;
exports.login = login;
