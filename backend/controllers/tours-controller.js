const HttpError = require('../models/http-error');
const {validationResult} = require('express-validator');
const Tour = require('../models/tours-model');
const User = require('../models/users-model');
const mongoose = require('mongoose');

const getTours = (req, res, next) => {
  // res.json(tours);
  let tours;
  try {
    tours = await Tour.find();
  } catch (error) {
    return next(
        new HttpError('Could not get tour.', 500),
    );
  }
  if (!tours || tours.length === 0) {
    return next(
      new HttpError('Could not get any tour.', 404),
  );
  }
  return res.status(200).json({tours: tours.map((tour) => tour.toObject({getters: true}))});
};

const getTourByID = async (req, res, next) => {
  const tourId = req.params.tid;
  let tour;
  try {
    tour = await Tour.findById(tourId);
  } catch (error) {
    return next(
        new HttpError('Could not find a tour.', 500),
    );
  }
  // const tour = tours.find((t) => {
  //   return t.id === tourId;
  // });

  if (!tour) {
    return next(
        new HttpError('Could not find the tour with this id.', 404),
    );
  }
  return res.json(tour.toObject({getters: true}));
};

const createTour = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(
        new HttpError('Invalid information on request bodies.', 422),
    );
  }
  // eslint-disable-next-line max-len
  const {title, seats, price, overview, locations, policies, description} = req.body;
  const createdTour = new Tour({
    title,
    seats,
    price,
    overview,
    locations,
    policies,
    description,
    tourists: [],
  });

  try {
    await createdTour.save();
  } catch (error) {
    return next(new HttpError(
        'Could not create a tour.', 500,
    ));
  }
  return res.status(201).json(createdTour);
};

const updateTourByID = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(
        new HttpError('Invalid information on request bodies.', 422),
    );
  }
  // eslint-disable-next-line max-len
  const {title, seats, price, overview, locations, policies, description} = req.body;
  const tourId = req.params.pid;
  let tour;
  try {
    tour = await Tour.findById(tourId);
  } catch (error) {
    return next(
        new HttpError('Could not find a tour.', 500),
    );
  }
  tour.title = title;
  tour.seats = seats;
  tour.prices = price;
  tour.overview = overview;
  tour.locations = locations;
  tour.policies = policies;
  tour.description = description;
  try {
    await tour.save();
  } catch (error) {
    return next(
        new HttpError('Could not update tour.', 500),
    );
  }
  return res.status(200).json(tour.toObject({getters: true}));
};

const registerTour = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(
        new HttpError('Invalid information on request bodies.', 422),
    );
  }

  const {tourId, userId} = req.body;
  let user;
  try {
    user = await User.findById(userId);
  } catch (error) {
    return next(
        new HttpError('Could not find the user', 500),
    );
  }
  if (!user) {
    return next(
        new HttpError('Could not find the user with provided ID.', 404),
    );
  }

  let tour;
  try {
    tour = await Tour.findById(tourId);
  } catch (error) {
    return next(
        new HttpError('Could not find a tour.', 500),
    );
  }
  if (!tour) {
    return next(
        new HttpError('Could not find the tour with provided ID.', 404),
    );
  }

  try {
    const sess = await mongoose.startSession();
    sess.startTransaction();
    user.tours.push(tour);
    await user.save({session: sess});
    tour.tourists.push(user);
    await tour.save({session: sess});
    await sess.commitTransaction();
  } catch (error) {
    return next(
        new HttpError('Could not register a tour.', 500),
    );
  }
};

const cancelTour = async (req, res, next) => {
  const {tourId, userId} = req.body;
  let tour;
  try {
    tour = await Tour.findById(tourId).populate('tourists');
  } catch (error) {
    return next(
        new HttpError('Could not delete the tour', 500),
    );
  }
  if (!tour) {
    return next(
        new HttpError('Could not find a tour with that ID', 404),
    );
  }
  let user;
  try {
    user = await Tour.findById(userId).populate('tours');
  } catch (error) {
    return next(
        new HttpError('Could not delete the tour', 500),
    );
  }
  if (!user) {
    return next(
        new HttpError('Could not find the user with that ID', 404),
    );
  }

  try {
    const sess = await mongoose.startSession();
    sess.startTransaction();
    tour.tourists.pull(user);
    await tour.save({session: sess});
    user.tours.pull(tour);
    await user.save({session: sess});
    await sess.commitTransaction();
  } catch (error) {
    return next(
        new HttpError('Could not cancel a tour.', 500),
    );
  }
};

const deleteTourByID = async (req, res, next) => {
  const tourId = req.params.tid;
  let tour;
  try {
    tour = await Tour.findById(tourId);
  } catch (error) {
    return next(
        new HttpError('Could not find a tour with that ID', 404),
    );
  }
  try {
    await tour.remove();
  } catch (error) {
    return next(
        new HttpError('Could not delete the tour', 500),
    );
  }
  return res.status(200).json({message: 'Deleted tour.'});
};

exports.getTours = getTours;
exports.getTourByID = getTourByID;
exports.deleteTourByID = deleteTourByID;
exports.createTour = createTour;
exports.updateTourByID = updateTourByID;
exports.registerTour = registerTour;
