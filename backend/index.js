const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const HttpError = require('./models/http-error');
const mongoose = require('mongoose');
const app = express();
const port = 8000;
const url = 'mongodb+srv://<username>:<password>@the-traveller.9xhji.mongodb.net/the-traveller?retryWrites=true&w=majority';

// app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors());

const toursRoutes = require('./routes/tours-routes');
app.use('/tours', toursRoutes);

const usersRoutes = require('./routes/users-routes');
app.use('/users', usersRoutes);

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.use((req, res, next) => {
  const error = new HttpError('Could not find this route.', 404);
  throw error;
});

app.use((error, req, res, next) => {
  if (res.headerSent) {
    return next(error);
  }
  res.status(error.code || 500);
  res.json({message: error.message || 'Unknown error occurred!'});
});

mongoose
    .connect(url)
    .then(() => {
      app.listen(port, () => {
        console.log(`App: on port ${port}!`);
      });
    })
    .catch((error) => {
      console.log(error);
    });

