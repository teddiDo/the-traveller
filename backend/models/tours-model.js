const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const tourSchema = new Schema({
  title: {type: String, required: true},
  seats: {type: Number, required: true},
  price: {type: Number, required: true},
  overview: {type: String, required: true},
  locations: [{type: String}],
  policies: [{type: String}],
  description: {type: String, required: true},
  tourists: [{type: mongoose.Types.ObjectId, required: true, ref: 'User'}],
});

module.exports = mongoose.model('Tour', tourSchema);
