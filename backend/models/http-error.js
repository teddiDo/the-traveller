/* eslint-disable require-jsdoc */
// eslint-disable-next-line no-unused-vars
class HttpError extends Error {
  constructor(message, errorCode) {
    super(message);
    this.code = errorCode;
  }
}

module.exports = HttpError;
