const express = require('express');
const usersController = require('../controllers/users-controller');
const {check} = require('express-validator');
// const checkAuth = require('../middleware/check-auth');

// eslint-disable-next-line new-cap
const router = express.Router();

router.get('/', usersController.getUsers);

// router.get('/uid', );
// router.use(checkAuth);


router.post('/signup', [
  check('email').normalizeEmail().isEmail(),
  check('password').isLength({min: 8}),
], usersController.signUp);

router.post('/login', usersController.login);

// router.post('/register-tour/:tid', );

module.exports = router;
