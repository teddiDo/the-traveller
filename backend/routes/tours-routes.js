const express = require('express');
const {check} = require('express-validator');
const toursControllers = require('../controllers/tours-controller');
// eslint-disable-next-line new-cap
const router = express.Router();

router.get('/', toursControllers.getTours);

router.get('/:tid', toursControllers.getTourByID);
// check req.body
router.post('/',
    [
      check('title')
          .not()
          .isEmpty(),
      check('description').isLength({min: 10}),
    ],
    toursControllers.createTour);

router.put('/:tid', toursControllers.updateTourByID);
router.put('/register-tour', toursControllers.registerTour);
router.delete('/:tid', toursControllers.deleteTourByID);

module.exports = router;
