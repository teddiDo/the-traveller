const jwt = require('jsonwebtoken');
const HttpError = require('../models/http-error');


module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];
    if (!token) {
      throw new Error('Unauthenticated!');
    }
    const decodedToken = jwt.verify(token, 'this_is_a_secret_key');
    req.user = {
      userId: decodedToken.id,
      nickname: decodedToken.nickname,
      email: decodedToken.email,
    };
    next();
  } catch (error) {
    return next(
        new HttpError('Unauthenticated!', 401),
    );
  }
};
